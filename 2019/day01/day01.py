def part01():
    total_fuel = 0

    with open("input.txt", "r") as f:
        for mass in f:
            total_fuel += int(mass) // 3 - 2

    print(total_fuel)

def part02():
    total_fuel = 0

    with open("input.txt", "r") as f:
        for mass in f:
            mass_fuel = 0
            while int(mass) > 0:
                current_fuel = int(mass) // 3 - 2
                if current_fuel > 0:
                    mass_fuel += current_fuel
                mass = current_fuel
            total_fuel += mass_fuel

    print(total_fuel)

part02()
def part01():
    instructions = []
    with open("input.txt", "r") as f:
        for line in f:
            instructions = list(map(int, line.split(',')))

    instructions[1] = 12
    instructions[2] = 2

    x = 0
    while x < len(instructions):
        instruction = instructions[x]
        if instruction == 99:
            break

        val1 = instructions[instructions[x+1]]
        val2 = instructions[instructions[x+2]]
        final_pos = instructions[x+3]
        if instruction == 1:
            instructions[final_pos] = val1 + val2
        elif instruction == 2:
            instructions[final_pos] = val1 * val2

        x += 4

    print(instructions[0])

part01()
def is_valid(password):
    digits = [int(d) for d in str(password)]
    return contains_adjacent_digits(digits) and never_decreases(digits)

def contains_adjacent_digits(digits):
    return digits[0] == digits[1] or digits[1] == digits[2] or digits[2] == digits[3] or digits[3] == digits[4] or digits[4] == digits[5]

def never_decreases(digits):
    return digits[0] <= digits[1] and digits[1] <= digits[2] and digits[2] <= digits[3] and digits[3] <= digits[4] and digits[4] <= digits[5]

def get_num_passwords():
    valid_passwords = 0
    for p in range(240920, 789858):
        if is_valid(p):
            valid_passwords += 1
    return valid_passwords

print(get_num_passwords())
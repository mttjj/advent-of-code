import java.io.*;
import java.nio.file.*;
import java.util.*;

class Day2Part2 {
    public static void main(String[] args) throws IOException {
        List<String> allLines = Files.readAllLines(Paths.get("real-input.txt"));
        long checksum = 0;
        for (String line : allLines) {
            checksum+= getRowSum(line);
        }
        System.out.println(checksum);
    }
    
    private static long getRowSum(String row) {
        String[] values = row.split(" ");
        for (int i = 0; i < values.length; i++) {
            long val1 = Long.parseLong(values[i]);
            for (int j = 0; j < values.length; j++) {
                long val2 = Long.parseLong(values[j]);
                
                if (val1 == val2) {
                    continue;
                }
                
                long high = val1 > val2 ? val1 : val2;
                long low = val1 < val2 ? val1 : val2;
                if (high%low == 0) {
                    return high/low;
                }
            }
        }
        return 0;
    }
}
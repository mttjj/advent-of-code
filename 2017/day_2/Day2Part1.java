import java.io.*;
import java.nio.file.*;
import java.util.*;

class Day2Part1 {
    public static void main(String[] args) throws IOException {
        List<String> allLines = Files.readAllLines(Paths.get("real-input.txt"));
        long checksum = 0;
        for (String line : allLines) {
            checksum+= getRowSum(line);
        }
        System.out.println(checksum);
    }
    
    private static long getRowSum(String row) {
        String[] values = row.split(" ");
        long lowest = Long.MAX_VALUE;
        long highest = Long.MIN_VALUE;
        for (int i = 0; i < values.length; i++) {
            long value = Long.parseLong(values[i]);
            if (value < lowest) {
                lowest = value;
            }
            if (value > highest) {
                highest = value;
            }
        }
        
        return highest - lowest;
    }
}
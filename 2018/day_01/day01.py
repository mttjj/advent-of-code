def part01():
    frequency = 0

    with open('input.txt') as f:
        for line in f:
            if line[0] == '+':
                frequency += int(line[1:])
            else:
                frequency -= int(line[1:])
                
    return frequency

def part02():
    frequencies = [0]
    
    with open('input.txt') as f:
        frequency_changes = f.read().splitlines()
     
    while True:
        for frequency in frequency_changes:
            cur_frequency = frequencies[-1]
            if frequency[0] == '+':
                cur_frequency += int(frequency[1:])
            else:
                cur_frequency -= int(frequency[1:])
            
            if cur_frequency in frequencies:
                return cur_frequency
            else:
                frequencies.append(cur_frequency)

print(part01()) #516

print(part02()) #71892
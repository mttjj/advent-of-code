def part01():
    twos = 0
    threes = 0

    with open('input.txt') as f:
        for box_id in f:
            found_twos = False
            found_threes = False

            unique_chars = ''.join(set(box_id))
            for char in unique_chars:
                if found_twos and found_threes:
                    break

                char_count = box_id.count(char)
                if not found_twos and char_count == 2:
                    twos += 1
                    found_twos = True
                elif not found_threes and char_count == 3:
                    threes += 1
                    found_threes = True
                    
    return twos * threes

def part02():
    with open('input.txt') as f:
        box_ids = f.read().splitlines()
        
    for index, box_id1 in enumerate(box_ids):
        for box_id2 in box_ids[index + 1:]:
            differences = [i for i in range(len(box_id2)) if box_id1[i] != box_id2[i]]
            if len(differences) == 1:
                return box_id1.replace(box_id1[differences[0]], '')

print(part01()) #5478

print(part02()) #qyzphxoiseldjrntfygvdmanu
def get_claim(line):
    claim_num = line[:line.index(' @')]
    coordinates = line[line.index('@ ') + 2:]
    x_start, y_start = coordinates[:coordinates.index(': ')].split(',')
    width, height = coordinates[coordinates.index(': ') + 2:].split('x')
    return (claim_num, int(x_start), int(y_start), int(width), int(height))

def part01():
    claimed_coordinates = set()
    overlapping_coordinates = set()
    
    for line in open('input.txt'):
        claim_num, x_start, y_start, width, height = get_claim(line)
        
        for x in range(0, width):
            for y in range(0, height):
                coordinate_claim = (x_start + x, y_start + y)
                if coordinate_claim in claimed_coordinates:
                    overlapping_coordinates.add(coordinate_claim)
                else:
                    claimed_coordinates.add(coordinate_claim)

    return len(overlapping_coordinates)
        
print(part01()) #100595

def intersects_something(coordinates, all_coordinates):
    for other_coordinates in all_coordinates:
        if coordinates == other_coordinates:
            continue
        intersect = list(set(coordinates) & set(other_coordinates))
        if len(intersect) > 0:
            return True
    return False

def part02():
    all_coordinates = []
    for line in open('input.txt'):
        claim_num, x_start, y_start, width, height = get_claim(line)
        
        coordinates = []
        for x in range(0, width):
            for y in range(0, height):
                coordinates.append((x_start + x, y_start + y))
        all_coordinates.append(coordinates)
        
    for index, rectangle_claim in enumerate(all_coordinates):
        if not intersects_something(rectangle_claim, all_coordinates):
            return index + 1
    
print(part02()) #415